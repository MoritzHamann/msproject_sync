class MsprojectInfo < ActiveRecord::Base

  def self.update_or_create(attributes)
    assign_or_new(attributes).save
  end

  def self.assign_or_new(attributes)
    info = first || new
    info.assign_attributes(attributes)
    info
  end
end
