module XMLFileHelper
  class XMLFile
    @xml = nil
    @valid = false
    @tasks = nil
    @errors = nil
    @links = nil

    attr_reader :tasks, :valid, :errors, :links

    def initialize(file)
      # parse the file
      @xml = Nokogiri::XML(file)

      # check for errors
      if @xml.errors.empty?
        @valid = true
      else
        @valid = false
        @errors = @xml.errors
        return
      end

      # sort Task
      parseTasks()
      parseLinks()
    end

    def parseTasks
      if !@valid || @xml.nil?
        return false
      end

      allTasks = @xml.css("Project Task")
      if allTasks.empty?
        return false
      end

      # generate an array of XMLTask objects, sorted by the outline number
      @tasks = allTasks.map{|t| XMLTask.new(t)}
      @tasks = @tasks.sort
    end

    def parseLinks
      if !@valid || @xml.nil?
        return false
      end

      allTasks = @xml.css("Task")
      @links = allTasks.map {|t| XMLLink.new(t)}
    end

  end


  # class for the single tasks
  # we should probably store all attributes on the object, instead of using css all the time...
  class XMLTask
    @xmlTask = nil

    attr_reader :xmlTask

    def initialize(xmlTask)
      @xmlTask = xmlTask
    end

    def parentOutline
      outlinenumber = outlineNumber.split(".")
      return outlinenumber.slice(0, outlinenumber.length-1).join(".")
    end

    def outlineNumber
      return css("OutlineNumber").text
    end

    def uid
      return css("UID").text.to_i
    end

    def css(path)
      return @xmlTask.css(path)
    end

    def start_date
      return Date.parse(css("Start").text)
    end

    def due_date
      return Date.parse(css("Finish").text)
    end

    def estimated_hours
      duration = css("Work").text
      return nil if duration.empty?
      duration = DateTime.parse(css("Work").text)
      duration = duration.hour + duration.min.to_f/60
      return duration.round(2)
    end


    def <=>(other)
      outline_a = @xmlTask.css("OutlineNumber").text.split(".").map{|x| x.to_i}
      outline_b = other.css("OutlineNumber").text.split(".").map{|x| x.to_i}
      max_length = [outline_a.length, outline_b.length].min

      (0...max_length).each do |i|
        if outline_a[i] != outline_b[i]
          return outline_a[i] <=> outline_b[i]
        end
      end
      return outline_a.length <=> outline_b.length
    end
  end

  class XMLLink
    @xmlLink = nil
    @from_uid = nil
    @to_uid = nil

    def initialize(xmlTask)
      @xmlLink = xmlTask.css("PredecessorLink")
      @from_uid = @xmlLink.css("PredecessorUID").text.to_i
      @to_uid = xmlTask.css("UID").text.to_i
    end

    def css(path)
      return @xmlLink.css(path)
    end

    def from
      return @from_uid
    end

    def to
      return @to_uid
    end
  end

end
