class MsprojectUploadsController < ApplicationController
  require_relative '../helpers/XML_file_helper.rb'
  include XMLFileHelper

  before_filter :set_project

  def index
    all_issues = @project.issues.includes(:msprojectInfo)
    @known_issues = {}
    all_issues.each do |i|
      if !i.msprojectInfo.try(:uid).nil?
        @known_issues[i.msprojectInfo.uid] = i
      end
    end

    # a hash map of the custom fields associated with each tracker
    # key is the tracker id, value an array of accosiated custom_fields
    @custom_fields = {}
    @project.trackers.each do |t|
      @custom_fields[t.id] = t.custom_fields
    end
  end


  def uploadMSProjectFile
    # make sure we have received the xml content from the file
    if params[:uploadFile].nil? || params[:uploadFile].empty?
      respond_to do |format|
        format.json {
          render :json => {
            errorText: 'No file uploaded'
          }
        }
      end
      return
    end

    # get Tracker
    if params[:tracker_id].nil?
      respond_to do |format|
        format.json {render json: {errorText: "Tracker not given"}}
      end
      return
    end
    tracker = Tracker.where(id: params[:tracker_id]).first
    if tracker.nil?
      respond_to do |format|
        format.json {render json: {erroText: "Tracker not found, wrong ID"}}
      end
      return
    end

    # parse xml file
    xml = XMLFile.new(params[:uploadFile])

    # test if the xml parsing was successfull
    if !xml.valid
      respond_to do |format|
        format.json {
          render :json => {
            errorText: 'Error while parsing the XML File',
            errorDetails: xml.errors.to_json
          }
        }
      end
      return
    end


    # get all issues for this project, with their uid by performing an left join
    # on msproject_issue_infos
    known_issues = Issue.includes(:msprojectInfo)
                      .where(project_id: @project.id)
                      #joins("LEFT JOIN `msproject_issue_infos` ON `msproject_issue_infos`.`issue_id` = `issues`.`id`")
                      #.select("issues.*, msproject_issue_infos.uid, issue_id")
                      #.where(project_id: @project.id)


    # make a hash for the already inserted issues with outline number as key.
    # needed to get correct parent id
    inserted_issues = {}

    # since the tasks are sorted in the XMLFile object, we can insert them successively
    errors = []
    xml.tasks.each do |t|
      e = updateIssueFromTask(t, known_issues, inserted_issues, tracker)
      if !e.nil?
        errors.push(e)
      end
    end

    # create or update issue relations
  relation_errors = []
    xml.links.each do |r|
      e = updateIssueRelation(r, known_issues)
      if !e.nil?
        relation_errors.push(e)
      end
    end

    # depending on flag, delete all issues of this project, which are not in inserted_issues
    if params[:deleteOldIssues] && params[:deleteOldIssues] == "true"
      inserted_ids = inserted_issues.map {|o, i| i.id}
      issue_ids_to_remove = known_issues.select {|i| !inserted_ids.include? i.id}
      Issue.where({project_id: @project.id, id: issue_ids_to_remove}).destroy_all
    end


    # return a success or the errors at the end
    if errors.empty?
      respond_to do |format|
        format.json {
          render :json => {
            success: true
          }
        }
      end
    else
      respond_to do |format|
        format.json {render :json => {errorText: 'Folgende Fehler sind aufgetreten', errorDetails: errors.to_json}}
      end
    end
    return
  end

=begin
      # create Project
      projectIdentifier = projectName.gsub(/[^a-z0-9A-Z]/, "-").downcase # replace non alphanumeric value with "-"
      projectIdentifier = projectIdentifier.gsub(/-{2,}/, "-") #replace multiple hyphens with single one
      projectIdentifier = projectIdentifier.gsub(/^[^a-zA-Z0-9]+|[^a-zA-Z0-9]+$/, "") # replace all non alphanumeric values at start and end with empty string

      project = Project.where(identifier: projectIdentifier).first_or_initialize
      project.name = projectName
      if !project.save
        flash[:error] = "Projekt konnte nicht gespeichert werden"
        redirect_to :action => 'index'
        return
      end

=end


  def updateIssueFromTask(task, known_issues, inserted_issues, tracker)
    # task information
    uid = task.uid
    subject = task.css("Name").text.empty? ? "No Name given" : task.css("Name").text
    start_date = task.start_date
    due_date = task.due_date
    parent = task.parentOutline.empty? ? nil : inserted_issues[task.parentOutline]
    #estimated_hours = task.estimated_hours

    # the issue which is going to be updated
    new_issue = known_issues.select {|i| i.msprojectInfo.try(:uid) == uid}.first || Issue.new
    if !new_issue.new_record?
      new_issue.reload
    end

    new_issue.tracker = tracker
    new_issue.subject = subject
    new_issue.start_date = start_date
    new_issue.due_date = due_date
    new_issue.parent = parent
    new_issue.project = @project
    new_issue.author = User.current
    #new_issue.estimated_hours = estimated_hours

    if new_issue.save
      # create entry in the msproject_issue_infos table
      info = new_issue.msprojectInfo || new_issue.create_msprojectInfo
      info.update(uid: uid)
      inserted_issues[task.outlineNumber] = new_issue
      known_issues.push(new_issue)
      return nil
    else
      return new_issue.errors
    end

  end


  def updateIssueRelation(relation, known_issues)
    # make sure both issues exist
    from_issue = known_issues.select {|i| i.msprojectInfo.try(:uid) == relation.from}.first
    to_issue = known_issues.select {|i| i.msprojectInfo.try(:uid) == relation.to}.first
    if from_issue.nil? || to_issue.nil?
      return "Unknown Issue IDs while creating IssueRelations"
    end

    new_relation = IssueRelation.where(issue_from_id: from_issue.id, issue_to_id: to_issue.id, relation_type: "blocks").first || IssueRelation.new
    new_relation.issue_from_id = from_issue.id
    new_relation.issue_to_id = to_issue.id
    new_relation.relation_type = "blocks"
    new_relation.save
    return nil
  end


  # helper method to set the project on each request
  def set_project
    if !params[:project_id].nil?
      begin
        @project = Project.find(params[:project_id])
      rescue
        @project = nil
      end
    end

    if @project.nil?
      respond_to do |format|
        format.json { render json: { errorText: 'No Project ID given or found'}}
        format.html {flash[:error]="No Project ID given or not found"; render_404;}
      end
    end
  end

end
