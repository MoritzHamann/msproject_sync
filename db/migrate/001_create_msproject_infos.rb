class CreateMsprojectInfos < ActiveRecord::Migration
  def change
    create_table :msproject_infos do |t|
      t.integer :issue_id, :index
      t.integer :uid
    end
  end
end
