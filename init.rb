Redmine::Plugin.register :msproject_sync do
  name 'Msproject Sync plugin'
  author 'Moritz Hamann'
  description 'Importing and exporting from MS Project XML Files'
  version '0.0.1'
  url 'http://example.com/path/to/plugin'
  author_url 'http://example.com/about'

  permission :msproject, { :msproject_uploads => [:index] }, :public => false
  menu :project_menu, :msproject, {controller: 'msproject_uploads', action: 'index'}, :param => :project_id, caption: "MS Project Upload"

  # load monkey patches
  require 'patch/IssueIncludeMSProjectInfo'

end
