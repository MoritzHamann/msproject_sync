# Plugin's routes
# See: http://guides.rubyonrails.org/routing.html


scope "/projects/:project_id" do
  get "/ms_project", :controller => 'msproject_uploads', :action => 'index'
  post "/ms_project/upload", :controller => 'msproject_uploads', :action => 'uploadMSProjectFile'
end
