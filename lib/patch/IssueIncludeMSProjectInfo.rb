# monkey patch the Issue model, to have a relation to our UID Table
module IssueMSProjectInfo
  def self.included(base)
    base.class_eval do
      has_one :msprojectInfo, :dependent => :destroy
    end
  end
end


if !Issue.included_modules.include? IssueMSProjectInfo
  Issue.send(:include, IssueMSProjectInfo)
end
