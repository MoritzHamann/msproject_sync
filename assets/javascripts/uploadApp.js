app = angular.module('uploadApp', []);

app.controller('MSProjectFileController', ["$scope", "$window", "$http", function($scope, $window, $http) {
  $scope.projectname = "";
  $scope.tasks = [];
  $scope.flash = {
    show : false,
    class: '',
    text: ''
  }
  $scope.loading = false;
  $scope.xmlFile = undefined;
  $scope.selectedTracker = $window.trackers[0];
  $scope.deleteOldIssues = false;
  $scope.win = $window;


  $scope.readXMLFile = function() {
    // hide status box
    $scope.flash.show = false;

    if (event.target.files && event.target.files[0]) {
      var file = event.target.files[0];
      var reader = new FileReader();

      reader.onload = function(e){
        var xmlString = e.target.result;
        var xmlParsed = undefined;

        // try to parse file
        try {
          var xmlParsed = jQuery.parseXML(xmlString);
        } catch (error) {
          $scope.flash.show = true;
          $scope.flash.class = "error";
          $scope.flash.text = "Error while reading the XML File";
          return false;
        }
        $scope.projectname = jQuery(xmlParsed).find('Project > Name').text();

        // map the tasks into js objects
        $scope.tasks = jQuery(xmlParsed).find('Project Task').map(function(index, item){
          var tmp = {};
          jitem = jQuery(item);
          tmp.name = jitem.find("Name").text();
          tmp.startDate = new Date(jitem.find("Start").text());
          tmp.endDate = new Date(jitem.find("Finish").text());
          tmp.work = jitem.find("Work").text();
          tmp.level = parseInt(jitem.find("OutlineLevel").text(), 10);
          tmp.levelArray =
          tmp.uid = parseInt(jitem.find("UID").text(),10);

          tmp.exists_already = $scope.win.known_issues[tmp.uid] !== undefined;
          return tmp;
        }).toArray();

        $scope.xmlString = xmlString;
        $scope.$apply();
      }
      // read the file
      reader.readAsText(file);
    } else {
      $scope.projectname = "";
      $scope.tasks = [];
      $scope.$apply();
    }
  }

  $scope.uploadFile = function() {
    $scope.loading = true;
    $scope.flash.show = false;
    $http({
      method  : 'POST',
      url     : $window.controller.action,
      data    : $.param({
        'uploadFile' : $scope.xmlString,
        'authenticity_token': $window.controller.token,
        'tracker_id': $scope.selectedTracker.id,
        'deleteOldIssues': $scope.deleteOldIssues
      }),
      headers : {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept' : 'application/json'
      }  // set the headers so angular passing info as form data (not request payload)
    })
    .then(function(response) {
      $scope.loading = false;
      if (response.errorText !== undefined){
        $scope.flash.show = true;
        $scope.flash.class= "error";
        $scope.flash.text = response.errorText;
      } else {
        $scope.flash.show = true;
        $scope.flash.class= "notice";
        $scope.flash.text = "Success";
        $scope.tasks = [];
      }
    }, function(response) {
      $scope.loading = false;
      $scope.flash.show = true;
      $scope.flash.class = "error";
      $scope.flash.text = "Server Error";
    });
  }

}]);

app.directive('fileChange', function() {
  return {
    restrict: 'A',
    link: function (scope, element, attrs) {
      var onChangeHandler = scope.$eval(attrs.fileChange);
      element.bind('change', onChangeHandler);
    }
  };
});

app.filter('makeRange', function() {
    return function(input) {
        var lowBound, highBound, step;
        switch (input.length) {
        case 1:
            lowBound = 0;
            highBound = parseInt(input[0], 10) - 1;
            step = 1;
            break;
        case 2:
            lowBound = parseInt(input[0], 10);
            highBound = parseInt(input[1], 10);
            step = 1;
            break;
        case 3:
            lowBound = parseInt(input[0], 10);
            highBound = parseInt(input[1], 10);
            step = parseInt(input[2], 10);
            break;
        default:
            return input;
        }
        var result = [];
        for (var i = lowBound; i <= highBound; i=i+step)
            result.push(i);
        return result;
    };
});
